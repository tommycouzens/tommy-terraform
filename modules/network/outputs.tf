output "id" {
  value = "${azurerm_virtual_network.main.id}"
}

output "name" {
  value = "${azurerm_virtual_network.main.name}"
}

output "nic" {
  value = "${azurerm_network_interface.main.id}"
}

output "vmip" {
  value = "${azurerm_public_ip.main.ip_address}"
}

output "subnet" {
  value = "${azurerm_subnet.internal.id}"
}
