variable "prefix" {
  default = "tommy"
}

variable "location" {
  default = "UKSouth"
}


variable "resource_group" {
  default = "tommy-terraform"
}