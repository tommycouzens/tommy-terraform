resource "azurerm_virtual_machine" "main" {
  name                  = "${var.prefix}-vm"
  location              = "${var.location}"
  resource_group_name   = "${var.resource_group}"
  network_interface_ids = ["${var.vnetwork_interface_id}"]
  vm_size               = "Standard_DS1_v2"

  delete_os_disk_on_termination = true

    connection {
        type     = "ssh"
        user     = "${var.admin_username}"
        private_key = "${file(var.sshkey)}"
    }

  provisioner "remote-exec" {
    inline = [
        "sudo yum install -y  httpd",
        "sudo service httpd start"
    ]
  }

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "hostname"
    admin_username = "${var.admin_username}"
  }
  os_profile_linux_config {
    disable_password_authentication = true
    ssh_keys {
        path     = "/home/${var.admin_username}/.ssh/authorized_keys"
        key_data = "${file("~/.ssh/id_rsa.pub")}"
}
  }
  tags = {
    environment = "staging"
  }
}