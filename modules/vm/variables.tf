variable "prefix" {
  default = "tommy"
}
variable "location" {
  default = "UKSouth"
}
variable "resource_group" {
  default = "tommy-terraform"
}
variable "admin_username" {
  default = "tommycouzens"
}

variable "vnetwork_interface_id" {
}

variable "sshkey" {}