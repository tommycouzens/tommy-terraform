data "template_file" "test_file" {
  template = "${file("${path.module}/test_file")}"
  vars = {
    vm_id = "${azurerm_virtual_machine.main.id}"
  }
}

resource "null_resource" "export_rendered_template" {
  provisioner "local-exec" {
    command = "cat > test_file.rendered <<EOL\n${data.template_file.test_file.rendered}\nEOL"
  }
}