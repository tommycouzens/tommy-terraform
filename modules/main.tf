module "network" {
  source = "./network"

  prefix = "${var.prefix}"
  location = "${var.location}"
  resource_group = "${azurerm_resource_group.main.name}"
}

module "vm" {
  source = "./vm"

  prefix = "${var.prefix}"
  location = "${var.location}"
  resource_group = "${azurerm_resource_group.main.name}"
  vnetwork_interface_id = "${module.network.nic}"
  sshkey = "${var.sshkey}"
  admin_username = "${var.admin_username}"
}