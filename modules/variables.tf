variable "prefix" {
  default = "tommy"
}
variable "resource_group" {
  default = "tommy-group"
}

variable "location" {
  default = "UKSouth"
}

variable "admin_username" {
  default = "tommycouzens"
}

variable "sshkey" {
  default = "~/.ssh/id_rsa"
}