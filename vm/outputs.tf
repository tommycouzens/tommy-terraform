output "publicIP" {
    value = "${azurerm_public_ip.main.ip_address}"
}

output "Login" {
    value = "ssh ${var.admin_username}@${azurerm_public_ip.main.ip_address}"
}