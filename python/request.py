import requests



response = requests.get(
    'http://localhost/get',
    headers={'Accept': 'application/json'},
)

# print(response.content)

response = requests.post(
    'http://localhost/post',
    json={'Name': 'Tommy'},
    params={'t': 'avengers'},
    headers={'Accept': 'application/json'},
)
print(response.content)

print(response.request)
print(response.request.url)
print(response.request.body)

response = requests.get(
    'http://localhost/basic-auth/tommy/admin',
    headers={'Accept': 'application/json'},
)

# print (response.content)