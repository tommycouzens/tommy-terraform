name_every_year.py generates a list of movies from the years 1980 to 2019 and outputs unsorted.json of them, and sorted.json which is the same but sorted by metacritis/imdb rating.  

To run name_every_year.py you need to export the apikey  
`export OMDB_API_KEY=6efa0329`

Then you can run it with:  
`python  name_every_year.py <movie title>`

You can specify a score metric as an extra parameter with two options, i.e:  
```
python  name_every_year.py avengers Metascore
python  name_every_year.py avengers imdbRating
``