import requests

response = requests.get(
    'http://localhost/cookies/set',
    params={'Name': 'Tommy'},
    headers={'Accept': 'text/plain'},
)

print(response.content)

requests.get(
    'http://localhost/cookies/set',
    params={'Age': '23'},
    headers={'Accept': 'text/plain'},
)

response = requests.get(
    'http://localhost/cookies',
    headers={'Accept': 'application/json'},
)

print(response.content)