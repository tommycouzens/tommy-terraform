import requests
import os
import sys
import json
from time import time
URL = "http://www.omdbapi.com/?"


## Example python  name_every_year.py avengers 'Metascore'

def set_api_key():
    #If you are using python3 uncomment this line and comment out the one below
	os.environ['OMDB_API_KEY'] = input('Set API Key: ')
    # os.environ['OMDB_API_KEY'] = raw_input('Set API Key: ')

def get_movie_by_year(movie_name, year = ""):
    if os.environ.get('OMDB_API_KEY') is None:
        set_api_key()
    key = os.environ['OMDB_API_KEY']    
    return requests.get(URL + 'apikey=' + key + '&t=' + movie_name + '&y=' + year)



def get_movie_across_years(movie_name):
    result_arr = []
    for year in range(1980,2020):
        movie = get_movie_by_year(movie_name, str(year)).json()
        if movie['Response'] == 'True':
          result_arr.append(movie)
    return result_arr


def true_response(movie_json):
    if movie_json['Response'] == 'True':
        return True
    else:
        return False

def get_movie_across_years_with_filter(movie_name):
    result_arr = []
    for year in range(1980,2020):
        movie = get_movie_by_year(movie_name, str(year)).json()
        result_arr.append(movie)
    return list(filter(true_response, result_arr))

def movie_score(movie, metric='imdbRating'):
    try: 
        score = float(movie[metric])
    except:
        score = 0
    return score

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print( "you messed up")
    else:
        title = sys.argv[1]
        movies = get_movie_across_years(title)
        # movies = get_movie_across_years_with_filter(sys.argv[1])
        with open('unsorted.json', 'w') as outfile:
            json.dump(movies, outfile, indent=4)

        # sort by numbers
        if len(sys.argv) > 2:
            sorted_movies = sorted(movies, key = lambda movie: movie_score(movie, sys.argv[2]), reverse = True)
        else:
            sorted_movies = sorted(movies, key = lambda movie: movie_score(movie, 'Metascore'), reverse = True)
        #move N/A result to the bottom

        with open('sorted.json', 'w') as outfile:
            json.dump(sorted_movies, outfile, indent=4)
        print("There are", len(movies), "movie titles containing the world", title, "between 1980 and 2019")
