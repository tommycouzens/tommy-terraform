import requests
import os
import sys
URL = "http://www.omdbapi.com/?"


def get_movie(movie_name):
    if os.environ.get('OMDB_API_KEY') is None:
        set_api_key()
    key = os.environ['OMDB_API_KEY']    
    return requests.get(URL + 'apikey=' + key + '&t=' + movie_name)

def set_api_key():
    #If you are using python3 uncomment this line and comment out the one below
	os.environ['OMDB_API_KEY'] = input('Set API Key: ')
    # os.environ['OMDB_API_KEY'] = raw_input('Set API Key: ')

def help():
    print('''
    python {} "<movie_name>" 
    '''.format(
        sys.argv[0]
        )
    )


if __name__ == "__main__":
    if len(sys.argv) < 2:
        help()
    else:
        print(get_movie(sys.argv[1]).text)
