import requests
import sys
from getpass import getpass
from requests.auth import HTTPBasicAuth

print(sys.argv)
response = requests.get('https://api.github.com/user', auth=HTTPBasicAuth('AicyDC', getpass()))

print(response.content)
print()


# timeouts:

from requests.exceptions import Timeout

try:
    response = requests.get('https://api.github.com', timeout=1)
except Timeout:
    print('The request timed out')
else:
    print('The request did not time out')