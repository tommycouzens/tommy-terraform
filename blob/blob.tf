resource "azurerm_storage_account" "storage_account" {
  name                     = "${var.prefix}storageaccount123"
  resource_group_name      = "${azurerm_resource_group.group.name}"
  location                 = "${var.region}"
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    Name = "${var.prefix}"
  }
}

resource "azurerm_storage_container" "container" {
  name                  = "${var.prefix}-storage-container"
  resource_group_name   = "${azurerm_resource_group.group.name}"
  storage_account_name  = "${azurerm_storage_account.storage_account.name}"
  container_access_type = "blob"
}

# resource "azurerm_storage_blob" "blob" {
#   name = "blob"

#   resource_group_name    = "${azurerm_resource_group.group.name}"
#   storage_account_name   = "${azurerm_storage_account.storage_account.name}"
#   storage_container_name = "${azurerm_storage_container.container.name}"

# }