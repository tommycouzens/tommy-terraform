resource "azurerm_resource_group" "group" {
  name     = "${var.prefix}"
  location = "${var.region}"
}