variable "prefix" {
  default = "tommy"
}

variable "region" {
  default = "UKSouth"
}